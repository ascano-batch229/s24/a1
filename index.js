// Exponent Operator

	// number3 solution
	
	let num = 3;
	let getCube = num**3;

	console.log(getCube);


// Template Literals

	// number4 solution
	let cubeSentence = `The cube of ${num} is ${getCube}`;
	console.log(cubeSentence);

// Array Destructuring
const address = ["258", "Washington Ave NW", "California", "90011"];

let [address1,address2,address3,address4] = address;

console.log(`I live at ${address1} ${address2}, ${address3} ${address4}`);



// Object Destructuring
const animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in"
}

let {name, species, weight, measurement} = animal;

console.log(`${name} was a ${species}. He Weigthed at ${weight} with a measurement of ${measurement}.`)

// Arrow Functions
let numbers = [1, 2, 3, 4, 5];

let forEachNum = numbers.forEach((num) => {
	console.log(num);
})

// Javascript Classes

class Dog {
	constructor(name,age,breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

let dog1 = new Dog("Bocchi", 15, "Shiba");
let dog2 = new Dog("Mikado", 5, "Husky");

console.log(dog1);
console.log(dog2);

